require 'conf-dev'

_LOVED_VERSION = '1.0-alpha'
_WIN32_ANSI_ESCAPE_SUPPORT = true

local http = require 'connection'
local utils = require 'utils'

http.dispatch(http.get('http://bitbucket.org/rude/love/downloads/love-11.3-win32.zip',  'Love 11.3 Win32')) {
  ok = function (result)
    utils.logger('ok')
    download = io.open('../love.zip', 'wb+')
    download:write(result)
    download:flush()
    download:close()
  end,
  err = function (result)
    utils.err(result)
  end
}