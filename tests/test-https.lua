require 'conf-dev'

local http
local ltn12 = require 'ltn12'
local https = require 'ssl.https'

local _, status, headers, request = https.request {
    url = 'http://bitbucket.org/rude/love/downloads/love-11.3-win32.zip',
    headers = {
        'User-Agent: Lua/5.1 Loved/1.0-alpha'
      },
    sink = ltn12.sink.file(io.open('..\\love-11.3-win32-http.zip', 'w+')),
    mode = "client",
    protocol = "any",
    cafile = "ca_certificates/moz_cacert.pem",
    verify = "peer",
    options = "all",
    redirect = "false"
  }

print(body, status, request)

for k, v in pairs(headers) do
  print(k, v)
end