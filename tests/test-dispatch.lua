require 'conf-dev'

_LOVED_VERSION = '1.0-alpha'
--_DEBUG = true
--_WIN32_ANSI_ESCAPE_SUPPORT = true

local http = require 'connection'
local utils = require 'utils'

http.dispatchall({google = http.get('www.google.es'), gnu = http.get('gnu.org'), wikipedia = http.get('www.wikipedia.es'), youtube = http.get('www.youtube.es')}) {
  ok = function(name)
    print(name .. ' was completed')
  end,
  err = function(name, err_msg)
    print(name .. ' ' .. err_msg)
  end
}