require 'conf-dev'

local utils = require 'utils'

local args = {'option', '--opt1', 'o', '--opt2', 'hello', 'world'}

local parsed_args = utils.getargs(args, {['opt1'] = '?', ['opt2'] = 2})

for opt, opt_val in pairs(parsed_args) do
  if type(opt_val) == 'table' then
    print(opt)
    for key, val in pairs(opt_val) do
      print('>', key, val)
    end
  else
    print(opt, opt_val)
  end
end