local lua_version = _VERSION:match("%d+%.%d+")

_DEBUG = true
--_WIN32_ANSI_ESCAPE_SUPPORT = true

package.path = 'lua_modules/share/lua/' .. lua_version .. '/?.lua;' .. package.path
package.path = 'lua_modules/share/lua/' .. lua_version .. '/?/init.lua;' .. package.path
package.cpath = 'lua_modules/lib/lua/' .. lua_version .. '/?.so;' .. package.cpath
package.cpath = 'lua_modules/lib/lua/' .. lua_version .. '/?.dll;' .. package.cpath