package = "Loved"
version = "1.0-alpha"
description = {
    summary = "A Love2D Project Creator and Distributor Toolkit",
    issues_url = "https://gitlab.com/NEKERAFA/loved/issues",
    maintainer = "Rafael Alcalde Azpiazu <nekerafa@gmail.com>",
    license = "MIT",
    labels = {"toolkit", "love2d"}
}
supported_platforms = {"unix", "win32"}
dependencies = {
    "luafilesystem ~> 1.7.0-2",
    "luasec ~> 0.9-1",
    "luasocket ~> 3.0rc1-2",
    "penlight ~> 1.7.0-1",
    "lua >= 5.1"
}