local pl_app = require 'pl.app'
local pl_dir = require 'pl.dir'
local pl_path = require 'pl.path'
local pl_utils = require 'pl.utils'
local pl_pretty = require 'pl.pretty'
local lfs = require 'lfs'
local http = require 'connection'
local utils = require 'utils'

_LOVED_VERSION = '1.0-alpha'
_LOVE_VERSION = '11.3'

local function print_version()
  utils.printf("Love2D Distribuitor v%s", _LOVED_VERSION)
end

local function print_new_project_usage()
  print_version()
  utils.echo()
  utils.echo([[Usage: loved new [options] <project_name>
  Creates a new Love2D project, downloading the lasted version of Love2D and Git and installing locally

Options:
  --love <path> Sets Love2D installation path. If no path added, uses a system call to invocate Love2D
  --git <path>  Sets Git installation path. If no path added, uses a system call to invocate Git
  --no-scm      Not uses SCM software

Typing "loved help" print information about all commands]])

  os.exit(0)
end

local function mkdir(...)
  local args = {...}
  local path_folder = ''
  
  for _, item in ipairs(args) do
    path_folder = pl_path.join(path_folder, item)
  end
  
  utils.logger('Create folder %s', path_folder)
  local ok, msg = pl_dir.makepath(path_folder)
  if not ok then utils.err(msg) end
end

local function download_love(path_temp, path_loved)
  local system = pl_app.platform()
  local url = ''
  local arch = ''
  local filename = ''
  
  if system == 'Linux' then
    filename = 'love-11.3-i686.AppImage'
    url = 'https://github.com/love2d/love/downloads/love-' .. _LOVE_VERSION .. '-i686.AppImage'
    arch = 'AppImage (Linux i686)'
  elseif system == 'Windows' then
    filename = 'love-11.3-win32.zip'
    url = 'https://github.com/love2d/love/downloads/love-' .. _LOVE_VERSION .. '-win32.zip'
    arch = 'Win32'
  end
  
  if arch == '' then
    utils.error('sorry, %s platform is not supported yet', arch)
    os.exit(-1)
  end
    
  return http.dispatch(http.get(url, 'Love2D ' .. _LOVE_VERSION .. ' ' .. arch)) {
    ok = function(content)
      local file = io.open(pl_path.join(path_temp, filename), 'wb+')
      file:write(content)
      file:flush()
      file:close()
      return true, filename
    end,
    err = function(msg)
      return false, msg
    end
  }
end

local function extract_love(path_temp, filename, path_loved)
  local path_zipped = pl_path.join(path_temp, filename)
  local path_unzipped = pl_path.splitext(path_zipped)
  local command = string.format('%s x -o"%s" "%s"', pl_path.join('win32', '7za.exe'), path_unzipped, path_zipped)
  
  utils.logger('Extracting %s', path_zipped)
  utils.debug(command)
  local ok, code, msg = pl_utils.execute(command)
  if not ok then 
    utils.err('cannot extract %s (%s %s)', path_zipped, tostring(code), tostring(msg))
    os.exit(-1)
  end
  
  utils.logger('Installing %s', path_unzipped)
  local folder_love = pl_path.splitext(filename)
  local path_love = pl_path.join(path_loved, 'love')
  ok, msg = os.rename(pl_path.join(path_unzipped, folder_love), path_love)
  
  if not ok then utils.err('cannot copy %s to %s (%s)', path_unzipped, path_love, msg) end
end

local function move_love(path_temp, filename, path_loved)
  local _, ext = pl_path.splitext(filename)
  local path_love = pl_path.join(path_loved, 'love')
  local path_exe_temp = pl_path.join(path_temp, filename)
  local path_exe = pl_path.join(path_love, 'love.' .. ext)
  
  mkdir(path_love)
  
  local ok, msg = os.rename(path_exe_temp, path_exe)

  if not ok then utils.err('cannot copy %s to %s (%s)', path_exe_temp, path_exe, msg) end
end

local function install_love(path_temp, filename, path_loved)
  local system = pl_app.platform()
  
  if system == 'Windows' then 
    extract_love(path_temp, filename, path_loved)
  else
    move_love(path_temp, filename, path_loved)
  end
end

local function check_love()
  local exists, _, version = pl_utils.executeex('love --version')
  version = string.gsub(version, '\n', '')
  return exists, version
end

local function check_git()
  local exists, _, version = pl_utils.executeex('git --version')
  version = string.gsub(version, '\n', '')
  return exists, version
end

local function check_scm()
  local exists_git, git_version = check_git()
  if not exists_git then return false end
  
  local scms = {'git', false}
  local choice = 0
  local max_choices = 1

  while choice == 0 do
    utils.logger('What SCM do you want to use?')
    utils.logger('----------------------------')
    utils.logger('1. ' .. git_version .. ' (installed)')
    utils.logger('2. Not use any SCM')
    
    utils.logger('----------------------------')
    utils.logger('Selection: ', {endline = false})
  
    choice = tonumber(io.stdin:read())
    
    if choice == nil or choice > max_choices then choice = 0 end
  end
    
  return scms[choice]
end

local function loved_mkconf(love, downloaded, git, path_loved)
  local configuration = { version = _LOVED_VERSION }
  
  if type(love) == 'boolean' then
    local _, version = check_love()
    configuration.love = {on_system = true, version = version}
  elseif type(love) == 'string' then
    configuration.love = {path = love}
    if downloaded then configuration.love.version = _LOVE_VERSION end
  end
  
  if type(git) == 'boolean' then
    configuration.scm = {uses = 'git', on_system = true}
  elseif type(git) == 'string' then
    configuration.scm = {uses = 'git', path = git}
  end
  
  local conf_path = pl_path.join(path_loved, 'conf.lua')
  pl_pretty.dump(configuration, conf_path)
end

local function love_mkconf(path_game)
  local conf, msg_file = io.open(pl_path.join(path_game, 'conf.lua'), 'w+')
  if not conf then utils.err(msg_file) end
  
  conf:write([[ package.path = ']] .. string.gsub(pl_path.join('libraries', '?.lua'), '\\', '\\\\') .. [[;]] .. string.gsub(pl_path.join('libraries', '?', '?.lua'), '\\', '\\\\') .. [[;' .. package.path
  
function love.conf(t)
  t.window.width = 640
  t.window.height = 480
end
]])
  conf:flush()
  conf:close()
end

local function love_make_main_script(path_game)
  local main, msg_file = io.open(pl_path.join(path_game, 'main.lua'), 'w+')
  if not main then utils.err(msg_file) end
  
  main:write([[function love.draw()
  love.graphics.print("Hello world", 10, 10)
end
]])
  main:flush()
  main:close()
end

local function set_git(git, path_game)
  if type(git) == 'boolean' then git = 'git' end
  
  local curdir = lfs.currentdir()
  lfs.chdir(path_game)
  
  local ok, code, msg = pl_utils.execute(git .. ' init')
  if not ok then utils.err('cannot create git folder (%s %s)', code, msg) end
  
  local gitignore, msg_file = io.open('.gitignore', 'w+')
  if not gitignore then utils.err(msg_file) end

  gitignore:write([[.loved/tmp/*
build/*
libraries/*
]])
  gitignore:flush()
  gitignore:close()
  
  ok, code, msg = pl_utils.execute(git .. ' add .')
  if not ok then utils.err('cannot add gitignore (%s %s)', code, msg) end
  
  ok, code, msg = pl_utils.execute(git .. ' commit -m "First commit"')
  if not ok then utils.err('cannot commit changes (%s %s)', code, msg) end
  
  lfs.chdir(curdir)
end

local function new_project(...)
  local args = {...}
  local n_args = #args
  local path_game = 'love2d-game'
  
  if n_args == 1 and args[1] == 'help' then
    print_new_project_usage()
  elseif n_args >= 1 then
    args = utils.getargs(args, {['love'] = '?', ['git'] = '?'})
    path_game = args[1] or path_game
    path_game = string.gsub(pl_path.normpath(path_game), '/', '\\')
  end
  
  if args['git'] and args['no-scm'] then
    utils.err('cannot set "git" and "no-scm" options together\n', { exit = false });
    print_new_project_usage()
    os.exit(-1)
  end
  
  mkdir(path_game)
  
  local path_loved = pl_path.join(path_game, '.loved')
  mkdir(path_loved)

  local downloaded_love = false
  if not args['love'] then
    utils.logger()
    utils.logger('Checking Love2D...')
    
    local love, version = check_love()
    
    if love then
      utils.logger('Detected %s', version)
      args['love'] = true
    else
      local path_temp = pl_path.join(path_loved, 'tmp')
      mkdir(path_temp)
      utils.logger('No Love2D installation detected, installing Love2D ' .. _LOVE_VERSION)
      local ok, filename_or_msg = download_love(path_temp, path_loved)
      
      if not ok then
        utils.err(filename_or_msg)
      end
      
      install_love(path_temp, filename_or_msg, path_loved)
      pl_dir.rmtree(path_temp)
      
      if pl_app.platform() == 'Windows' then
        args['love'] = pl_path.join('.loved', 'love', 'love.exe')
      else
        args['love'] = pl_path.join('.loved', 'love', filename_or_msg)
      end
      
      downloaded_love = true
    end
  end
  
  if not args['git'] and not args['no-scm'] then
    utils.logger()
    local scm = check_scm()
    if scm == 'git' then args['git'] = true end
  end
  
  loved_mkconf(args['love'], downloaded_love, args['git'], path_loved)
  
  mkdir(path_game, 'libraries')
  mkdir(path_game, 'assets', 'sprites')
  mkdir(path_game, 'assets', 'sounds')
  love_mkconf(path_game)
  love_make_main_script(path_game)
  
  if args['git'] then set_git(args['git'], path_game) end
  
  return path_game
end

local function load_conf()
  local conf_file, msg = io.open(pl_path.join('.loved', 'conf.lua'))
  if not conf_file then utils.err(msg) end
  
  local conf_tbl = conf_file:read('*a')
  return pl_pretty.read(conf_tbl)
end

local function get_release(release)
  if not release or release == '' then return 0 end
  return 0.5
end

local function compare_version(version)
  local mayor_conf, minor_conf, release_conf = string.match(version, "(%d+)%.(%d+)%-?(%w*)")
  mayor_conf = tonumber(mayor_conf)
  minor_conf = tonumber(minor_conf)
  
  local mayor, minor, release = string.match(_LOVED_VERSION, "(%d+)%.(%d+)%-?(%w*)")
  mayor = tonumber(mayor)
  minor = tonumber(minor)
  
  return (mayor - mayor_conf) * 100 + minor - minor_conf + get_release(release) - get_release(release_conf)
end

local function update_configuration(conf)
end

local function check_configuration()
  local configuration = load_conf()
  if not configuration then utils.err('cannot load configuration file') end
  
  local error_checking = 'error checking configuration file (%s)'
  
  if not configuration.version then utils.err(error_checking, 'cannot get loved version') end
  
  local version = compare_version(configuration.version)
  if version > 0 then
    utils.warn('The configuration is for Loved %s. Please update Loved to get all compatible features', configuration.version)
  elseif version < 0 then
    configuration = update_configuration(configuration)
    utils.logger('Updated configuration to Loved %s', _LOVED_VERSION)
  end
  
  if not configuration.love then utils.err(error_checking, 'cannot get love configuration') end
  
  local love = 'love'
  if not configuration.love.on_system then love = configuration.love.path end
  
  local git = false
  if configuration.scm then
    if not configuration.scm.uses then utils.err(error_checking, 'cannot get scm configuration') end
    
    if configuration.scm.uses == 'git' then
      if configuration.scm.on_system then
        git = 'git'
      else
        git = configuration.scm.path
      end
    end
  end
  
  return love, git
end

local function run_project(path_game)
  local curdir = lfs.currentdir()
  if path_game then lfs.chdir(path_game) end
  local love = check_configuration()
  pl_utils.execute(love .. ' ' .. pl_path.join('.', ''))
  if path_game then lfs.chdir(curdir) end
end

local function print_usage()
  print_version()
  utils.echo()
  utils.echo([[Usage: loved [command] [command-options] [arguments]

Commands:
  new     Creates new Love2D proyect
  play    Run a Love2D project
  version Print the version of Loved
  help    Print this message

Typing "loved [command] help" print information about a command]])
  os.exit(0)
end

--- Prints all given arguments as error
local function print_error_args(args)
  local s_args = ''
  local n_args = #args
  
  for i, arg in ipairs(args) do
    s_args = s_args .. arg
    if i < n_args then
      s_args = s_args .. ' '
    end
  end
  
  utils.err('"%s" not recognized\n', s_args)
  print_usage()
  os.exit(-1)
end

-- Main entry point
local function loved()
  local n_arg = #arg
  
  if n_arg == 0 or arg[1] == 'help' then
    print_usage()
  elseif arg[1] == 'new' then 
    local path_game = new_project(unpack(arg, 2))
    utils.logger('Project created in %s', path_game)
    utils.logger('Running project')
    run_project(path_game)
  elseif arg[1] == 'play' then
    run_project(pl_path.normcase(pl_path.normpath(arg[2])))
  elseif arg[1] == 'version' then
    print_version()
  else
    print_error_args(arg)
  end
end

loved()
