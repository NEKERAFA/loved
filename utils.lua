local app = require 'pl.app'

-- These codes are for set printing styles in the output text
-- This must be concatenate as '^ESC[*code*m
local codes = {
  -- Reset all style and colour text
  reset = 0,
  -- text styles
  styles = {
    Bold      = 1,
    Underline = 4,
    Inverse   = 7
  },
  -- foreground colours
  -- background colours are code+10
  fg = {
    Black       = 30,
    DarkRed     = 31,
    DarkGreen   = 32,
    DarkYellow  = 33,
    DarkBlue    = 34,
    DarkMagenta = 35,
    DarkCyan    = 36,
    DarkGray    = 90,
    Red         = 91,
    Green       = 92,
    Yellow      = 93,
    Blue        = 94,
    Magenta     = 95,
    Cyan        = 96,
    Gray        = 37,
    White       = 97
  }
}

local function getarch()
  local arch = ''
  
  if app.platform() == 'Windows' then
    arch = os.getenv('PROCESSOR_ARCHITECTURE')
  else
    local uname_out = io.popen('uname -m')
    arch = uname_out:read('*l')
    uname_out:close()
  end
  
  return arch
end

local function getluaversion()
  local lua_version = _VERSION:match("%d+%.%d+")
  return lua_version
end

local function print_unix_text(str, params)
  local formats = ''
  
  if params.fg or params.bg or params.style then
    formats = formats .. '\27['
    
    if params.style and codes.style[params.style] then
      formats = formats .. tostring(codes.style[params.style])
    end
    
    if params.fg and codes.fg[params.fg] then
      if params.style then formats = formats .. ';' end
      
      formats = formats .. tostring(codes.fg[params.fg])
    end
    
    if params.bg and codes.fg[params.bg] then
      if params.style or params.fg then formats = formats .. ';' end
      
      formats = formats .. tostring(codes.fg[params.bg] + 10)
    end
    
    formats = formats .. 'm'
  end
  
  io.write(formats .. str)
  
  if params.fg or params.bg or params.style then
    io.write('\27[' .. tostring(codes.reset) .. 'm')
  end
  
  if params.endline then
    io.write('\n')
  end
end

local function print_win32_text(str, params)
  if _WIN32_ANSI_ESCAPE_SUPPORT then
    print_unix_text(str, params)
  else
    io.write(str)
    if params.endline then io.write('\n') end
  end
end

local function echo(str, params)
  if params == nil then params = {} end
  if params.endline == nil then params.endline = true end
  if str == nil then str = '' end

  if app.platform() == 'Windows' then
    print_win32_text(str, params)
  else
    print_unix_text(str, params)
  end
end

local function printf(msg, ...)
  local args = {...}
  local n_args = #args
  local params = {}
  
  if n_args > 0 and type(args[1]) == 'table' then
    params = args[1]
    args = {unpack(args, 2)}
    n_args = n_args - 1
  end
  
  if n_args > 0 then msg = string.format(msg, unpack(args)) end

  echo(msg, params)
end

local function datemark()
  if _DEBUG then
    echo(os.date('!%a %d %b %Y %X'), {fg = 'DarkYellow', endline = false})
    echo(' - ', {endline = false})
  end
end

local function info(msg, ...)
  local args = {...}
  local n_args = #args
  if n_args > 0 then msg = string.format(msg, unpack(args)) end
  
  datemark()
  
  if _DEBUG then echo('info - ', {endline = false}) end
  printf(msg, ...)
end

local function deb(msg, ...)
  if _DEBUG then
    local args = {...}
    local n_args = #args
    if n_args > 0 then msg = string.format(msg, unpack(args)) end
    
    datemark()
    
    printf('debug - %s', {fg = 'DarkGray'}, msg)
  end
end

local function warn(msg, ...)
  local args = {...}
  local n_args = #args
  if n_args > 0 then msg = string.format(msg, unpack(args)) end
  datemark()
  local token = ':'
  if _DEBUG then token = ' -' end
  printf('warning%s %s', {fg = 'DarkYellow'}, token, msg)
end

local function err(msg, ...)
  local args = {...}
  local n_args = #args
  local exit = -1
  local show_traceback = false
  
  if n_args > 0 then
    if type(args[1]) == 'table' then
      exit = args[1].exit or exit
      show_traceback = args[1].show_traceback or show_traceback
      msg = string.format(msg, unpack(args, 2))
    else
      msg = string.format(msg, unpack(args))
    end
  end
  
  datemark()
  
  local token = ':'
  if _DEBUG then token = ' -' end
  printf('error%s %s', {fg = 'DarkRed'}, token, msg)
  
  if exit then
    print(debug.traceback())
    os.exit(exit)
  end
end

local function getargs(args, options)
  local arg = 1
  local n_args = #args
  local parsed_args = {}
  
  while arg <= n_args do
    if string.find(args[arg], '^%-%-') then
      local option_name = string.sub(args[arg], 3)
      local n_options = options[option_name] or 0
      if (type(n_options) == 'number') and (n_options < 0) then n_options = 0 end
      
      if (type(n_options) == 'number') and (n_options == 0) then
        parsed_args[option_name] = true
        arg = arg+1
      elseif (type(n_options) == 'string') and (n_options == '?') then
        if arg+1 <= n_args then
          if string.find(args[arg+1], '^%-%-') then
            parsed_args[option_name] = true
            arg = arg+1
          else
            parsed_args[option_name] = args[arg+1]
            arg = arg+2
          end
        else
          parsed_args[option_name] = true
          arg = arg+1
        end
      elseif type(n_options) == 'number' then
        parsed_args[option_name] = {}
        local option = 1
        local next_arg = arg+1
      
        while (next_arg <= n_args) and (option <= n_options) do
          if string.find(args[next_arg], '^%-%-') then break end
          
          table.insert(parsed_args[option_name], args[next_arg])
          option = option+1
          next_arg = next_arg+1
        end
      
        if (next_arg > n_args+1) or (option <= n_options) then
          error(string.format('cannot parse "%s" option (expected %i params, got %i)', option_name, n_options, option-1))
        else
          arg = next_arg
        end
      else
        error(string.format('bad argument #%i (expected number or string, got %s)', arg, type(n_options)))
      end
    else
      table.insert(parsed_args, args[arg])
      arg = arg+1
    end
  end
  
  return parsed_args
end

return { echo = echo, printf = printf, datemark = datemark, logger = info, debug = deb, warn = warn, err = err, getarch = getarch, getluaversion = getluaversion, getargs = getargs }