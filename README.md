# Loved

A Love2D Project Creator and Distributor Toolkit

## Table of Contents

 - [Referece](#referece)
 - [Dependencies](#dependencies)
 - [Instalation](#instalation)
 - [Specs](#specs)

## Referece

- **new** &lt;name of proyect&gt;

Creates a new Love2D proyect. Loved will download the lastest version of Love2D and install it locally, as the SCM (prefered Git), initializing the repository folder, adding files ingnore and setting in order to upload content.

- **version**

Prints the current version of Loved

- **help**
  
Prints the usage message onto the console

*work in progress...*

If you want to know more information about a command you can type **&lt;command&gt;** help and Loved will print the lastest usage about the command.

## Dependencies

- [LuaFileSystem](https://keplerproject.github.io/luafilesystem/) *1.7.0-2* or above
- [LuaSec](https://github.com/brunoos/luasec) *0.9-1* or above
- [LuaSocket](http://w3.impa.br/~diego/software/luasocket/) *3.0rc1-2* or above
- [Penlight](https://stevedonovan.github.io/Penlight/api/manual/01-introduction.md.html) *1.7.0-1* or above

## Instalation

*work in progress...*

## Specs

*work in progress...*
